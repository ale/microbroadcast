AC_DEFUN([AX_CHECK_PORTAUDIO], [
  found=false
  AC_PATH_PROG([PKG_CONFIG], [pkg-config])
  if test x"$PKG_CONFIG" != x; then
    PORTAUDIO_LIBS=`$PKG_CONFIG --libs portaudio-2.0`
    if test $? -eq 0; then
      PORTAUDIO_CFLAGS=`$PKG_CONFIG --cflags portaudio-2.0`
      found=true
    fi
  fi
  if test x"$found" = xfalse; then
    AC_MSG_ERROR([portaudio library not found])
  fi
  AC_SUBST([PORTAUDIO_LIBS])
  AC_SUBST([PORTAUDIO_CFLAGS])
])
