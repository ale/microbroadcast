AC_DEFUN([AX_CHECK_OGG], [
  OGG_LIBS=
  OGG_CFLAGS=
  AC_PATH_PROG([PKG_CONFIG], [pkg-config])
  if test x"$PKG_CONFIG" != x; then
    OGG_LIBS=`$PKG_CONFIG --libs ogg`
    if test $? -eq 0; then
      OGG_CFLAGS=`$PKG_CONFIG --cflags ogg`
    fi
  fi

  _ogg_cflags="$CFLAGS"
  _ogg_libs="$LIBS"
  CFLAGS="$CFLAGS $OGG_CFLAGS"
  LIBS="$LIBS $OGG_LIBS"

  found=true
  AC_CHECK_HEADER([ogg/ogg.h], [], [found=false])
  AC_CHECK_LIB([ogg], [ogg_stream_init], [], [found=false])

  CFLAGS="$_ogg_cflags"
  LIBS="$_ogg_libs"
  if test x"$found" = xtrue; then
    AC_DEFINE(HAVE_OGG, 1, [Define if OGG library is available])
  fi
  AC_SUBST([OGG_LIBS])
  AC_SUBST([OGG_CFLAGS])
])
