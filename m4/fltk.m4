AC_DEFUN([AX_CHECK_FLTK], [
  found=false
  AC_PATH_PROG([FLTK_CONFIG], [fltk-config])
  if test x"$FLTK_CONFIG" != x; then
    FLTK_LDFLAGS=`$FLTK_CONFIG --use-images --ldflags`
    FLTK_LIBS="$FLTK_LDFLAGS"
    FLTK_CFLAGS=`$FLTK_CONFIG --use-images --cxxflags`
    found=true
  fi
  if test x"$found" = xfalse; then
    AC_MSG_ERROR([FLTK library not found])
  fi
  AC_SUBST([FLTK_LIBS])
  AC_SUBST([FLTK_CFLAGS])
])
