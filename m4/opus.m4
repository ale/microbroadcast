AC_DEFUN([AX_CHECK_OPUS], [
  OPUS_LIBS=
  OPUS_CFLAGS=
  AC_PATH_PROG([PKG_CONFIG], [pkg-config])
  if test x"$PKG_CONFIG" != x; then
    OPUS_LIBS=`$PKG_CONFIG --libs opus`
    if test $? -eq 0; then
      OPUS_CFLAGS=`$PKG_CONFIG --cflags opus`
    fi
  fi

  _opus_cflags="$CFLAGS"
  _opus_libs="$LIBS"
  CFLAGS="$CFLAGS $OPUS_CFLAGS"
  LIBS="$LIBS $OPUS_LIBS"

  found=true
  AC_CHECK_HEADER([opus/opus.h], [], [found=false])
  AC_CHECK_LIB([opus], [opus_encode], [], [found=false])

  CFLAGS="$_opus_cflags"
  LIBS="$_opus_libs"
  if test x"$found" = xtrue; then
    AC_DEFINE(HAVE_OPUS, 1, [Define if OPUS codec is available])
  fi
  AC_SUBST([OPUS_LIBS])
  AC_SUBST([OPUS_CFLAGS])
])
