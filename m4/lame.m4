AC_DEFUN([AX_CHECK_LAME], [
  found=true
  AC_CHECK_HEADER([lame/lame.h], [], [found=false])
  AC_CHECK_LIB([mp3lame], [lame_init], [], [found=false])
  if test x"$found" = xtrue; then
    AC_DEFINE(HAVE_LAME, 1, [Define if Lame mp3 library is available])
  fi
])
