AC_DEFUN([AX_CHECK_URIPARSER], [
  URIPARSER_LIBS=
  URIPARSER_CFLAGS=
  AC_PATH_PROG([PKG_CONFIG], [pkg-config])
  if test x"$PKG_CONFIG" != x; then
    URIPARSER_LIBS=`$PKG_CONFIG --libs liburiparser`
    if test $? -eq 0; then
      URIPARSER_CFLAGS=`$PKG_CONFIG --cflags liburiparser`
    fi
  fi

  _uriparser_cflags="$CFLAGS"
  _uriparser_libs="$LIBS"
  CFLAGS="$CFLAGS $URIPARSER_CFLAGS"
  LIBS="$LIBS $URIPARSER_LIBS"

  found=true
  AC_CHECK_HEADER([uriparser/Uri.h], [], [found=false])
  AC_CHECK_LIB([uriparser], [UriParseA], [], [found=false])

  CFLAGS="$_uriparser_cflags"
  LIBS="$_uriparser_libs"
  AC_SUBST([URIPARSER_LIBS])
  AC_SUBST([URIPARSER_CFLAGS])
])
