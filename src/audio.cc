/*

  audio.cc
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>

#include "audio.h"

#define PA_SAMPLE_TYPE paInt16
#define FRAMES_PER_BUFFER 1024

using namespace std;

namespace microb {

Capture::Capture() : ring_buffer_(new RingBuffer()), stream_(nullptr) {}

Capture::~Capture() {
  close();
  if (stream_) {
    done_.wait();
    Pa_CloseStream(stream_);
  }
}

int Capture::portaudio_callback(const void *inputBuffer, void *outputBuffer,
                                unsigned long framesPerBuffer,
                                const PaStreamCallbackTimeInfo *timeInfo,
                                PaStreamCallbackFlags statusFlags,
                                void *userData) {
  Capture *cap = static_cast<Capture *>(userData);
  return cap->callback(inputBuffer, outputBuffer, framesPerBuffer, timeInfo,
                       statusFlags);
}

void Capture::finished_callback(void *userData) {
  Capture *cap = static_cast<Capture *>(userData);
  cap->stream_finished();
}

util::Status Capture::open(const Params &params) {
  // assert stream_ == nullptr;

  const PaDeviceInfo *device_info = Pa_GetDeviceInfo(params.device_index);
  if (!device_info) {
    return util::Status(ERR) << "no such audio device";
  }

  PaStreamParameters input_parameters;
  input_parameters.device = params.device_index;
  input_parameters.channelCount = params.channels;
  input_parameters.sampleFormat = PA_SAMPLE_TYPE;
  input_parameters.suggestedLatency = device_info->defaultLowInputLatency;
  input_parameters.hostApiSpecificStreamInfo = nullptr;

  fprintf(stderr, "opening audio device: %d Hz, %d bits, %s\n",
          params.sample_rate, params.sample_size * 8,
          (params.channels == 1) ? "mono" : "stereo");

  int err = Pa_OpenStream(&stream_, &input_parameters, nullptr,
                          params.sample_rate, FRAMES_PER_BUFFER, paClipOff,
                          portaudio_callback, static_cast<void *>(this));
  if (err != paNoError) {
    return util::Status(ERR) << "Pa_Open: " << Pa_GetErrorText(err);
  }

  err = Pa_SetStreamFinishedCallback(stream_, finished_callback);
  if (err != paNoError) {
    return util::Status(ERR) << "Pa_SetStreamFinishedCallback: "
                            << Pa_GetErrorText(err);
  }

  cur_params_ = params;

  err = Pa_StartStream(stream_);
  if (err != paNoError) {
    return util::Status(ERR) << "Pa_StartStream: " << Pa_GetErrorText(err);
  }

  return util::Status(OK);
}

void Capture::close() {
  stop_.set();
  ring_buffer_->close();
}

int Capture::callback(const void *inputBuffer, void *outputBuffer,
                      unsigned long framesPerBuffer,
                      const PaStreamCallbackTimeInfo *timeInfo,
                      PaStreamCallbackFlags statusFlags) {
  if (ring_buffer_->write(static_cast<const char *>(inputBuffer),
                          framesPerBuffer * cur_params_.frame_size()) < 0) {
    fprintf(stderr, "ring_buffer write error\n");
    return paComplete;
  }
  if (stop_.is_set()) {
    return paComplete;
  }
  return paContinue;
}

void Capture::stream_finished() { done_.set(); }

void audio_initialize() {
  // One-off process initialization.
  int err = Pa_Initialize();
  if (err != 0) {
    exit(2);
  }

  // Populate input_devices with the list of Portaudio devices that
  // support recording.
  int num_devices = Pa_GetDeviceCount();
  for (int i = 0; i < num_devices; i++) {
    const PaDeviceInfo *info = Pa_GetDeviceInfo(i);
    const PaHostApiInfo *hostapi_info = Pa_GetHostApiInfo(info->hostApi);
    if (info->maxInputChannels > 0) {
      string name = string(hostapi_info->name) + ": " + info->name;
      DeviceInfo di = {i, name, info};
      input_devices.push_back(di);
    }
  }
}

void audio_close() { Pa_Terminate(); }

vector<DeviceInfo> input_devices;

} // namespace
