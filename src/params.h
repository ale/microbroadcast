// -*- mode: c++ -*-
/*

  params.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_params_H
#define __microb_params_H 1

#include "cfg.h"

namespace microb {

#define DEFAULT_SAMPLE_SIZE 2

class Params {
public:
  Params() : device_index(-1), sample_rate(0), channels(0), sample_size(0) {}

  Params(int sr, int chans)
      : sample_rate(sr), channels(chans), sample_size(DEFAULT_SAMPLE_SIZE) {}

  Params(const s_config &config)
      : device_index(config.device_index), sample_rate(config.sample_rate),
        channels(config.channels), sample_size(DEFAULT_SAMPLE_SIZE) {}

  int device_index;
  int sample_rate;
  int channels;
  int sample_size;

  int frame_size() const { return sample_size * channels; }
};

// EncodingParams are just params with a bitrate.
class EncodingParams : public Params {
public:
  EncodingParams() : Params() {}

  EncodingParams(const s_config &config)
      : Params(config), bitrate(config.bitrate) {}

  int bitrate;
};

} // namespace

#endif
