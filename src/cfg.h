// -*- mode: c++ -*-
/*

  cfg.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_cfg_H
#define __microb_cfg_H 1

#include <string>

#include "json.h"
#include "util.h"

namespace microb {

class s_config {
public:
  std::string codec;
  int sample_rate;
  int channels;
  int device_index;
  int bitrate;

  std::string http_url;
  std::string http_username;
  std::string http_password;

  s_config();
  s_config(const nlohmann::json &jc);

  s_config merge(const s_config &other);

  util::Status check();

  static s_config default_config();

  static std::string user_config_path();
  static util::Status read_file(const std::string &path, s_config *out);
  util::Status write_file(const std::string &path);

protected:
  nlohmann::json to_json() const;
};

} // namespace

#endif
