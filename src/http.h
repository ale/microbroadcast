// -*- mode: c++ -*-
/*

  http.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_http_H
#define __microb_http_H 1

#include <shout/shout.h>
#include <memory>
#include <string>

#include "cfg.h"
#include "ringbuffer.h"
#include "util.h"

using namespace std;

namespace microb {

// Uploader is the streaming HTTP client that is a consumer of the
// encoded audio data.
class Uploader : public util::JoinableThread {
public:
  Uploader(const string &url, const string &username, const string &password,
           const string &content_type)
      : util::JoinableThread(), url_(url), username_(username),
        password_(password), content_type_(content_type),
        ring_buffer_(new RingBuffer())
  {}

  Uploader(const s_config &config, const string &content_type)
      : Uploader(config.http_url, config.http_username, config.http_password,
                 content_type) {}

  ~Uploader() {
    close();
    wait();
  }

  void close();

  int write(const char *buf, unsigned int len) {
    if (done_.is_set()) {
      // Writes should fail if we have failed with a permanent error.
      return -1;
    }
    return ring_buffer_->write(buf, len);
  }

  util::Status status() {
    return saved_status_;
  }

protected:
  string url_;
  string username_;
  string password_;
  string content_type_;
  util::Flag stop_;
  unique_ptr<RingBuffer> ring_buffer_;
  char *errbuf_;
  util::Flag done_;
  util::Status saved_status_;
  
  virtual util::Status run();
  util::Status run_once();
};

void http_initialize();
void http_close();

} // namespace

#endif
