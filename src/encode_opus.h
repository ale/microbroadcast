// -*- mode: c++ -*-
/*

  encode_opus.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_encode_opus_H
#define __microb_encode_opus_H 1

#include <opus/opus.h>
#include <opus/opus_multistream.h>

#include "encode.h"
#include "encode_ogg.h"
#include "params.h"
#include "util.h"

namespace microb {

class OPUSEncoder : public Encoder, public OggStreamMixin {
public:
  OPUSEncoder()
      : packetno_(0), granulepos_(0), opus_frame_len_(0),
        buf_((char *)calloc(1, 4 * 4096)),
        enc_buf_((unsigned char *)calloc(1, 4 * 4096)) {}

  virtual ~OPUSEncoder() {
    close();
    free(buf_);
    free(enc_buf_);
  }

  virtual int buffer_size() const {
    return params_.frame_size() * opus_frame_len_;
  }

  virtual const char *content_type() const {
    return OggStreamMixin::content_type();
  }
  
  util::Status initialize(const EncodingParams &params) {
    params_ = params;

    // Find a valid OPUS frame size for the desired sample rate. Use a
    // 20ms frame size for rates above or equal to 22050Hz, 40ms for
    // rates below that value. Given the possible sample rates, this
    // will always result in an integer number of samples.
    opus_frame_len_ =
        params_.sample_rate * ((params_.sample_rate >= 22050) ? 20 : 40) / 1000;

    int err = 0;
    encoder_ = opus_encoder_create(params_.sample_rate, params_.channels,
                                   OPUS_APPLICATION_AUDIO, &err);
    if (err != OPUS_OK) {
      return util::Status(ERR) << "failure to initialize OPUS encoder: "
                               << opus_strerror(err);
    }

    opus_encoder_ctl(encoder_, OPUS_SET_BITRATE(params_.bitrate));

    return util::Status(OK);
  }

  virtual int encode(const char *input, int frames, char **output) {
    int w = 0;

    // Number of input frames should always be equal to
    // opus_frame_len_, but check nevertheless.
    if (frames == 0)
      return 0;

    w += flush_stream(buf_);

    // Encode the input and create an ogg_packet.
    int n = opus_encode(encoder_, (const opus_int16 *)input, frames, enc_buf_,
                        2048 * 4);
    if (n < 0) {
      return n;
    }
    ogg_packet op;
    op.b_o_s = 0;
    op.e_o_s = 0;
    op.granulepos = granulepos_;
    op.packetno = packetno_++;
    op.packet = enc_buf_;
    op.bytes = n;
    granulepos_ += opus_frame_len_;

    w += write_ogg_packet(buf_ + w, &op);

    *output = buf_;
    return w;
  }

  virtual void close() {
    if (encoder_ != nullptr) {
      ogg_stream_clear(&os_);
      opus_encoder_destroy(encoder_);
      encoder_ = nullptr;
    }
  }

protected:
  OpusEncoder *encoder_;
  int packetno_;
  ogg_int64_t granulepos_;
  int opus_frame_len_;
  char *buf_;
  unsigned char *enc_buf_;
  EncodingParams params_;
};

} // namespace

#endif
