// -*- mode: c++ -*-
/*

  util.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_util_H
#define __microb_util_H 1

#include <condition_variable>
#include <deque>
#include <mutex>
#include <string>
#include <thread>

#include <string.h>

#define OK 0
#define ERR -1

// Someone in Xlib defines Status as a macro...
#undef Status

namespace util {

// Result status for an operation, including an error message.
class Status {
public:
  Status() : code_(OK), msg_("") {}

  Status(int code) : code_(code), msg_("") {}

  Status(int code, const char *msg) : code_(code), msg_(msg) {}

  Status(const Status &x) : code_(x.code_), msg_(x.msg_) {}

  bool operator==(const Status &x) {
    return x.code_ == code_ && x.msg_ == msg_;
  }

  Status &operator<<(const std::string &s) {
    msg_ += s;
    return *this;
  }

  bool ok() const { return code_ == OK; }

  int code() const { return code_; }

  const char *what() { return msg_.c_str(); }

  static Status from_errno(int errnum) {
    return Status(errnum, strerror(errnum));
  }

protected:
  int code_;
  std::string msg_;
};

// A simple, blocking, thread-safe FIFO queue.
template <typename T> class Queue {
public:
  Queue() {}

  virtual ~Queue() {}

  void push(const T &value) {
    {
      std::lock_guard<std::mutex> lock(mx_);
      queue_.push_back(value);
    }
    cond_.notify_one();
  }

  T pop() {
    std::unique_lock<std::mutex> lock(mx_);
    cond_.wait(lock, [&] { return !queue_.empty(); });
    T value = queue_.front();
    queue_.pop_front();
    return value;
  }

protected:
  std::deque<T> queue_;
  std::mutex mx_;
  std::condition_variable cond_;
};

// Base class for a thread that can be started after construction time
// (to check for initialization errors), and that can be safely joined
// multiple times.
//
// The usage of a completion callback function avoids the need for a
// separate supervisor thread.
class JoinableThread {
public:
  typedef std::function<void(const Status &)> callback_t;

  JoinableThread() {}

  virtual ~JoinableThread() {}

  Status start_thread(const callback_t &done_cb) {
    // Pass done_cb as an argument to the thread, otherwise it will be
    // gone regardless of the lambda capture (?).
    //
    // TODO: catch exceptions here?
    thread_.reset(new std::thread(
        [&](callback_t done_cb_) {
          util::Status status = run();
          if (done_cb_) {
            done_cb_(status);
          }
        },
        done_cb));
    return Status(OK);
  }

  void wait() {
    std::lock_guard<std::mutex> lock(join_mx_);
    if (thread_) {
      thread_->join();
      thread_.reset(nullptr);
    }
  }

protected:
  virtual Status run() = 0;

  std::mutex join_mx_;
  std::unique_ptr<std::thread> thread_;
};

// Thread-safe boolean flag. The mutex acts as a memory barrier and
// synchronizes access to the data from different CPUs. The flag is
// meant to be set only once.
class Flag {
public:
  Flag() : value_(false) {}

  // Set the flag to true.
  void set() {
    std::lock_guard<std::mutex> lock(mx_);
    value_ = true;
  }

  // Reset the flag to false. Note that it is unsafe to call is_set()
  // and reset() in a loop, you probably want a condition_variable in
  // that case.
  void reset() {
    std::lock_guard<std::mutex> lock(mx_);
    value_ = false;
  }

  // Test whether the flag is set.
  bool is_set() {
    std::lock_guard<std::mutex> lock(mx_);
    return value_;
  }

protected:
  bool value_;
  std::mutex mx_;
};

// A Flag with a blocking call to wait for a value change.
class WaitableFlag : public Flag {
public:
  WaitableFlag() : Flag() {}

  void set() {
    {
      std::lock_guard<std::mutex> lock(mx_);
      value_ = true;
    }
    cond_.notify_all();
  }

  // Wait until the flag is set to true.
  void wait() {
    std::unique_lock<std::mutex> lock(mx_);
    cond_.wait(lock, [&] { return value_; });
  }

protected:
  std::condition_variable cond_;
};

} // namespace

#endif
