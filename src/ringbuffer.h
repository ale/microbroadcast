// -*- mode: c++ -*-
/*

  ringbuffer.h - simple SPSC thread-safe ring buffer.
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __ringbuffer_H
#define __ringbuffer_H 1

#include <condition_variable>
#include <mutex>
#include <thread>

#include <memory.h>
#include <stdlib.h>

#define DEFAULT_RINGBUFFER_SIZE 2 * 1024 * 1024

class RingBuffer {
public:
  RingBuffer() : RingBuffer(DEFAULT_RINGBUFFER_SIZE) {}

  RingBuffer(int size)
      : size_(size), write_avail_(size), read_pos_(0), write_pos_(0),
        eof_(false), overruns_(0) {
    buf_ = (char *)malloc(size);
  }

  virtual ~RingBuffer() { free(buf_); }

  void close() {
    {
      std::lock_guard<std::mutex> lock(mx_);
      eof_ = true;
    }
    cond_.notify_all();
  }

  int read(char *data, int n) {
    if (n <= 0) {
      return 0;
    }

    // Cap read size to buffer size.
    if (n > size_) {
      n = size_;
    }

    // Wait on the condition until there is enough data to
    // fulfill the read request.
    std::unique_lock<std::mutex> lock(mx_);
    cond_.wait(lock, [&] { return (eof_ || read_avail() > n); });
    if (eof_) {
      return -1;
    }

    // Copy the data to the output buffer, and increase the write
    // availability counter accordingly ("consume" the data).
    int rem = size_ - read_pos_;
    if (rem < n) {
      memcpy(data, buf_ + read_pos_, rem);
      memcpy(data + rem, buf_, n - rem);
      read_pos_ = n - rem;
    } else {
      memcpy(data, buf_ + read_pos_, n);
      read_pos_ += n;
    }
    write_avail_ += n;
    return n;
  }

  // Write data to the ring buffer. Writes always succeed, regardless
  // of the position of the read pointer: when the read pointer is
  // overrun, it moves forward accordingly. This makes it so the ring
  // buffer always contains the most recent data, in order.
  int write(const char *data, int n) {
    // Writing more than the buffer size isn't allowed.
    if (n < 0 || n > size_) {
      return -1;
    }
    if (n == 0) {
      return 0;
    }

    {
      std::lock_guard<std::mutex> lock(mx_);

      // See if there is enough free space for n bytes. If not, increase
      // the overrun counter and shift the read pointer forward (skip
      // bytes).
      int over = n - write_avail_;
      if (over > 0) {
        overruns_++;
        write_avail_ += over;
        read_pos_ += over;
        if (read_pos_ > size_) {
          read_pos_ -= size_;
        }
      }

      // Copy the data from the source buffer.
      int rem = size_ - write_pos_;
      if (rem < n) {
        memcpy(buf_ + write_pos_, data, rem);
        memcpy(buf_, data + rem, n - rem);
        write_pos_ = n - rem;
      } else {
        memcpy(buf_ + write_pos_, data, n);
        write_pos_ += n;
      }
      write_avail_ -= n;
    }

    cond_.notify_one();
    return n;
  }

protected:
  int read_avail() const { return size_ - write_avail_; }

  char *buf_;
  int size_;
  int write_avail_;
  int read_pos_;
  int write_pos_;
  volatile bool eof_;
  int overruns_;
  std::mutex mx_;
  std::condition_variable cond_;
};

#endif
