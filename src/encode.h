/* -*- mode: c++ -*- */
/*

  encode.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_encode_H
#define __microb_encode_H 1

#include "audio.h"
#include "cfg.h"
#include "params.h"
#include "util.h"

namespace microb {

// Abstract base class for encoders.
class Encoder {
public:
  Encoder() {}
  virtual ~Encoder() {}

  // Returns our preferred buffer size.
  virtual int buffer_size() const = 0;

  // Return the content-type for this encoder.
  virtual const char *content_type() const = 0;

  // Initialize the encoder.
  virtual util::Status initialize(const EncodingParams &params) = 0;

  // Sets output to point to a buffer that is valid until the next
  // call to 'encode'.
  virtual int encode(const char *input, int frames, char **output) = 0;

  virtual void close() = 0;

protected:
  EncodingParams params_;
};

#define ENC_MP3 "mp3"
#define ENC_VORBIS "vorbis"
#define ENC_OPUS "opus"

util::Status make_encoder(const s_config &config, Encoder **encoder_out);

} // namespace

#endif
