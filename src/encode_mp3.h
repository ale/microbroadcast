// -*- mode: c++ -*-
/*

  encode_mp3.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_encode_mp3_H
#define __microb_encode_mp3_H 1

#include <lame/lame.h>

#include "encode.h"
#include "params.h"

#define DEFAULT_MP3_BITRATE 128000

namespace microb {

class MP3Encoder : public Encoder {
public:
  MP3Encoder() : handle_(nullptr) {
    bufsz_ = 1024;
    buf_ = (char *)malloc(bufsz_);
  }

  virtual ~MP3Encoder() {
    close();
    free(buf_);
  }

  virtual const char *content_type() const { return "audio/mpeg"; }

  virtual int buffer_size() const { return 4096; }

  util::Status initialize(const EncodingParams &params) {
    params_ = params;

    handle_ = lame_init();

    lame_set_num_channels(handle_, params_.channels);
    lame_set_in_samplerate(handle_, params_.sample_rate);
    lame_set_out_samplerate(handle_, params_.sample_rate);
    lame_set_brate(handle_, params_.bitrate);

    // Fix some quality-related parameters and optimizations.
    lame_set_quality(handle_, 2);
    lame_set_asm_optimizations(handle_, MMX, 1);
    lame_set_asm_optimizations(handle_, SSE, 1);

    int err = lame_init_params(handle_);
    if (err < 0) {
      handle_ = nullptr;
      return util::Status(ERR) << "liblame initialization error";
    }
    return util::Status(OK);
  }

  int encode(const char *input, int frames, char **output) {
    int rc;
    check_buffer(frames);
    if (params_.channels > 1) {
      rc = lame_encode_buffer_interleaved(handle_, (short int *)input, frames,
                                          (unsigned char *)buf_, bufsz_);
    } else {
      rc = lame_encode_buffer(handle_, (const short int *)input,
                              (const short int *)input, frames,
                              (unsigned char *)buf_, bufsz_);
    }
    *output = buf_;
    return rc;
  }

  void close() {
    if (handle_ != nullptr) {
      lame_close(handle_);
      handle_ = nullptr;
    }
  }

protected:
  // Return minimum safe size for a buffer that holds the output of
  // encoding the given number of samples. Worst-case estimate
  // according to lame.h.
  int min_bufsize(int samples) { return 7200 + (samples + (samples + 3) / 4); }

  // Resize the output buffer if necessary.
  void check_buffer(int samples) {
    int minsz = min_bufsize(samples);
    if (bufsz_ < minsz) {
      buf_ = (char *)realloc(buf_, minsz);
      bufsz_ = minsz;
    }
  }

  char *buf_;
  int bufsz_;
  lame_global_flags *handle_;
};

} // namespace

#endif
