/*

  ui_aux.cc
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <string>

#include "pipeline.h"
#include "ui.h"
#include "util.h"

#include <FL/fl_ask.H>

using namespace std;
using namespace microb;

// UI error callback function. Note that this is invoked from the
// pipeline thread, so it can't safely invoke top-level window
// functions (like an alert). To overcome this limitation, we ask the
// main thread to run fl_alert on our behalf using Fl::awake.
//
// We don't have a good way to pass the util::Status to the second
// callback, so we store the error message in the heap, and expect the
// callback to free it.
static void ui_show_error(void *user_data) {
  const char *errmsg = (const char *)user_data;
  app->set_broadcast_state(false);
  fl_alert("%s", errmsg);
  free(user_data);
}

int ui_error_callback(util::Status status) {
  char *errmsg = (char *)malloc(strlen(status.what()) + 8);
  strcpy(errmsg, "Error: ");
  strcpy(errmsg + 7, status.what());
  Fl::awake(ui_show_error, errmsg);
  return 0;
}

static void ui_set_broadcast_state(void *user_data) {
  long state = (long)user_data;
  app->set_broadcast_state(state == STATE_RUNNING);
}

void ui_state_callback(int state) {
  Fl::awake(ui_set_broadcast_state, (void *)((long)state));
}

// The global quit callback.
void quit_cb(Fl_Menu_ *menu, void *arg) { app->quit(); }

void set_choice_by_value(Fl_Choice *w, const void *value) {
  const Fl_Menu_Item *menu = w->menu();
  for (int i = 0; i < w->size(); i++) {
    if (menu[i].user_data() == value) {
      w->value(i);
      break;
    }
  }
}

void set_choice_by_value(Fl_Choice *w, long arg) {
  const Fl_Menu_Item *menu = w->menu();
  for (int i = 0; i < w->size(); i++) {
    if (menu[i].argument() == arg) {
      w->value(i);
      break;
    }
  }
}
