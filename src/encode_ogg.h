// -*- mode: c++ -*-
/*

  encode_ogg.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_encode_ogg_H
#define __microb_encode_ogg_H 1

#include <ogg/ogg.h>

namespace microb {

class OggStreamMixin {
public:
  OggStreamMixin() { ogg_stream_init(&os_, rand()); }

  virtual ~OggStreamMixin() { ogg_stream_clear(&os_); }

  virtual const char *content_type() const { return "application/ogg"; }

protected:
  ogg_stream_state os_;
  ogg_page og_;

  int flush_stream(char *buf) {
    int w = 0;
    while (ogg_stream_flush(&os_, &og_) > 0) {
      w += write_page(buf + w);
    }
    return w;
  }

  int write_ogg_packet(char *buf, ogg_packet *op) {
    int w = 0;
    ogg_stream_packetin(&os_, op);
    bool eos = false;
    while (!eos) {
      int err = ogg_stream_pageout(&os_, &og_);
      if (err == 0)
        break;
      w += write_page(buf + w);
      if (ogg_page_eos(&og_)) {
        eos = true;
      }
    }
    return w;
  }

  int write_page(char *buf) {
    int w = 0;
    memcpy(buf + w, og_.header, og_.header_len);
    w += og_.header_len;
    memcpy(buf + w, og_.body, og_.body_len);
    w += og_.body_len;
    return w;
  }
};

} // namespace

#endif
