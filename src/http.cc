/*

  http.cc
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "http.h"
#include "config.h"
#include <stdio.h>
#include <unistd.h>
#include <uriparser/Uri.h>

namespace microb {

class URL {
public:

  URL(const string& uristr) {
    UriParserStateA state;
    state.uri = &uri_;
    ok_ = (uriParseUriA(&state, uristr.c_str()) == URI_SUCCESS);
  }

  ~URL() {
    if (ok_) {
      uriFreeUriMembersA(&uri_);
    }
  }    

  bool ok() { return ok_; }
  
  string scheme() { return from_range(uri_.scheme, "http"); }
  
  string host() { return from_range(uri_.hostText, ""); }

  string path() { return from_list(uri_.pathHead, "/"); }

  int port() {
    string default_port = "80";
    if (scheme() == "https") {
      default_port = "443";
    }
    return atoi(from_range(uri_.portText, default_port).c_str());
  }
  
  string from_range(const UriTextRangeA &rng, const string& default_value) const {
    if (rng.first != nullptr && rng.afterLast != nullptr) {
      return string(rng.first, rng.afterLast);
    }
    return default_value;
  }

  string from_list(UriPathSegmentA *xs, const string& delim) const {
    UriPathSegmentStructA *head = xs;
    string accum;
    while (head) {
      accum += delim + from_range(head->text, "");
      head = head->next;
    }
    return accum;
  }

protected:
  UriUriA uri_;
  bool ok_;
};

void Uploader::close() {
  stop_.set();
  ring_buffer_->close();
}

// Detect retriable, transient network errors.
static bool is_temporary_error(int err) {
  switch (err) {
  // ?? Just guessing here...
  case SHOUTERR_NOCONNECT:
  case SHOUTERR_UNCONNECTED:
  case SHOUTERR_SOCKET:
    return true;
  default:
    return false;
  }
}

static int shout_format(const string& content_type) {
  if (content_type == "application/ogg") {
    return SHOUT_FORMAT_OGG;
  } else if (content_type == "audio/mpeg") {
    return SHOUT_FORMAT_MP3;
  }
  // Unreached.
  return SHOUT_FORMAT_MP3;
}
  
util::Status Uploader::run() {
  util::Status status;
  while (!stop_.is_set()) {
    status = run_once();
    if (!status.ok()) {
      break;
    }
  }
  saved_status_ = status;
  done_.set();
  return status;
}
  
util::Status Uploader::run_once() {
  util::Status status;

  URL url(url_);
  if (!url.ok()) {
    return util::Status(ERR) << "could not parse URL " << url_;
  }
  if (url.scheme() != "http" && url.scheme() != "https") {
    return util::Status(ERR) << "unsupported URL scheme " << url.scheme();
  }

  shout_t *handle = shout_new();
  if (handle == nullptr) {
    return util::Status(ERR) << "failed to initialize libshout";
  }
  
  shout_set_host(handle, url.host().c_str());
  shout_set_port(handle, (unsigned short)(url.port()));
  shout_set_mount(handle, url.path().c_str());
  fprintf(stderr, "shout: host=%s port=%d path=%s\n",
          url.host().c_str(),
          url.port(),
          url.path().c_str());

  shout_set_format(handle, shout_format(content_type_));
  shout_set_protocol(handle, SHOUT_PROTOCOL_HTTP);
  shout_set_agent(handle, PACKAGE_NAME "/" PACKAGE_VERSION);
  if (!username_.empty()) {
    shout_set_user(handle, username_.c_str());
  }
  // Libshout always wants a password (even if empty).
  shout_set_password(handle, password_.c_str());

  int err = shout_open(handle);
  if (err < 0) {
    status = util::Status(err) << "libshout error: " << shout_get_error(handle);
    shout_free(handle);
    return status;
  }

  int bufsz = 4096;
  char *buf = (char *)malloc(bufsz);
  
  while (!stop_.is_set()) {
    // Retrieve some data from the ring buffer.
    int n = ring_buffer_->read(buf, bufsz);
    if (n < 0) {
      status = util::Status(n) << "read error";
      break;
    }
    
    // Send it to libshout.
    fprintf(stderr, "shout_send(%d)\n", n);
    n = shout_send(handle, (unsigned char *)buf, n);
    if (n < 0) {
      if (is_temporary_error(n)) {
        // Sleep for a little while and return OK so the outer loop
        // will retry the connection.
        fprintf(stderr, "libshout error: %s, retrying...\n", shout_get_error(handle));
        usleep(200000);
        status = util::Status(OK);
      } else {
        status = util::Status(n) << "libshout error: " << shout_get_error(handle);
      }
      break;
    }
  }

  shout_close(handle);
  shout_free(handle);
  free(buf);

  return status;
}

void http_initialize() {
  shout_init();
}

void http_close() {
  shout_shutdown();
}

} // namespace
