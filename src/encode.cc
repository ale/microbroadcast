/*

  encode.cc
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "encode.h"
#include "config.h"

#if HAVE_LAME
#include "encode_mp3.h"
#endif
#if HAVE_OPUS && HAVE_OGG
#include "encode_opus.h"
#endif
#if HAVE_VORBIS && HAVE_OGG
#include "encode_vorbis.h"
#endif

namespace microb {

util::Status make_encoder(const s_config &config, Encoder **encoder_out) {
  EncodingParams params(config);
  util::Status status;

#if HAVE_LAME
  if (config.codec == ENC_MP3) {
    MP3Encoder *enc = new MP3Encoder();
    status = enc->initialize(params);
    if (!status.ok()) {
      delete enc;
    } else {
      *encoder_out = enc;
    }
    return status;
  }
#endif

#if HAVE_OPUS && HAVE_OGG
  if (config.codec == ENC_OPUS) {
    OPUSEncoder *enc = new OPUSEncoder();
    status = enc->initialize(params);
    if (!status.ok()) {
      delete enc;
    } else {
      *encoder_out = enc;
    }
    return status;
  }
#endif

#if HAVE_VORBIS && HAVE_OGG
  if (config.codec == ENC_VORBIS) {
    VorbisEncoder *enc = new VorbisEncoder();
    status = enc->initialize(params);
    if (!status.ok()) {
      delete enc;
    } else {
      *encoder_out = enc;
    }
    return status;
  }
#endif

  // Unknown codec type.
  return util::Status(ERR) << "unknown codec " << config.codec;
}

} // namespace
