# data file for the Fltk User Interface Designer (fluid)
version 1.0303 
header_name {.h} 
code_name {.cc}
decl {\#include "pipeline.h"} {public global
} 

decl {\#include "cfg.h"} {public global
} 

decl {\#include "encode.h"} {public global
} 

decl {\#include <FL/fl_ask.H>} {public global
} 

decl {\#include <FL/Fl_PNG_Image.H>} {public global
} 

Function {show_prefs_win_cb()} {open private
} {
  code {app->prefs_win->show();} {}
} 

decl {\#undef Status} {public global
} 

class AppUI {open
} {
  Function {make_window()} {open protected
  } {
    Fl_Window main_win {
      label MicroBroadcast open
      xywh {513 17 300 300} type Double hide resizable size_range {0 0 300 300}
    } {
      Fl_Button broadcast_btn {
        callback {if (broadcast_active_) {
	set_broadcast_state(false);
	pipeline_->stop();
} else {
	set_broadcast_state(true);
	pipeline_->start();
}}
        protected tooltip {Start / stop broadcast} xywh {0 20 300 300} labelfont 1 labelcolor 72 align 304 resizable
        code0 {broadcast_btn->image(rec_img_);}
      }
      Fl_Menu_Bar {} {open
        xywh {0 0 510 20}
      } {
        Submenu {} {
          label {&File} open
          xywh {0 0 70 20}
        } {
          MenuItem {} {
            label Preferences
            callback show_prefs_win_cb
            xywh {0 0 36 20} divider
          }
          MenuItem {} {
            label {&Quit}
            callback quit_cb
            xywh {0 0 36 20} shortcut 0x40071
          }
        }
      }
    }
  }
  Function {make_preferences_window()} {open protected
  } {
    Fl_Window prefs_win {
      label Preferences
      xywh {1 17 510 750} type Double hide resizable
    } {
      Fl_Group {} {
        label {Audio Options} open
        xywh {20 25 440 75} labelfont 2
      } {
        Fl_Choice pref_audio_device {
          label {Audio device:}
          protected xywh {140 30 320 20} down_box BORDER_BOX
          code0 {for (auto dev : microb::input_devices) { pref_audio_device->add(dev.name.c_str(), 0, 0, (void *)((long)(dev.index))); }}
        } {}
        Fl_Choice pref_sample_rate {
          label {Sample rate:} open
          protected xywh {140 55 320 20} down_box BORDER_BOX
        } {
          MenuItem {} {
            label 48000
            user_data 48000 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 44100
            user_data 44100 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 22050
            user_data 22050 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 11025
            user_data 11025 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 8000
            user_data 8000 user_data_type long
            xywh {0 0 455 20}
          }
        }
        Fl_Choice pref_channels {
          label {Channels:}
          protected xywh {140 80 320 20} down_box BORDER_BOX
        } {
          MenuItem {} {
            label 1
            user_data 1 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 2
            user_data 2 user_data_type long
            xywh {0 0 455 20}
          }
        }
      }
      Fl_Group {} {
        label {HTTP Options}
        xywh {20 125 440 90} labelfont 2 resizable
      } {
        Fl_Input pref_http_url {
          label {Stream URL:}
          protected xywh {140 131 320 24}
        }
        Fl_Input pref_http_username {
          label {Username:}
          protected xywh {140 161 320 24}
        }
        Fl_Input pref_http_password {
          label {Password:}
          protected xywh {140 191 320 24}
        }
      }
      Fl_Group {} {
        label {Encoding Options} open
        xywh {20 240 440 50} labelfont 2
      } {
        Fl_Choice pref_codec {
          label {Codec:} open
          protected xywh {140 245 320 20} down_box BORDER_BOX
        } {
          MenuItem {} {
            label MP3
            user_data ENC_MP3
            xywh {170 0 285 20}
          }
          MenuItem {} {
            label VORBIS
            user_data ENC_VORBIS
            xywh {170 0 285 20}
          }
          MenuItem {} {
            label OPUS
            user_data ENC_OPUS
            xywh {170 0 285 20}
          }
        }
        Fl_Choice pref_bitrate {
          label {Bitrate:} open
          protected xywh {140 270 320 20} down_box BORDER_BOX
        } {
          MenuItem {} {
            label 320k
            user_data 320000 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 256k
            user_data 256000 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 192k
            user_data 192000 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 128k
            user_data 128000 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 96k
            user_data 96000 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 64k
            user_data 64000 user_data_type long
            xywh {0 0 455 20}
          }
          MenuItem {} {
            label 48k
            user_data 48000 user_data_type long
            xywh {0 0 455 20}
          }
        }
      }
      Fl_Group {} {open
        xywh {30 305 430 25}
      } {
        Fl_Button {} {
          label Cancel
          callback {prefs_win->hide();
reset_prefs();}
          xywh {225 305 117 25} shortcut 0xff1b
        }
        Fl_Return_Button {} {
          label Apply
          callback {apply_prefs();}
          xywh {353 305 107 25}
        }
      }
    }
  }
  Function {AppUI(microb::PipelineManager *pipeline)} {open
  } {
    code {pipeline_ = pipeline;
broadcast_active_ = false;
rec_img_ = new Fl_PNG_Image("rec", rec_png_data_, sizeof(rec_png_data_));
stop_img_ = new Fl_PNG_Image("stop", stop_png_data_, sizeof(stop_png_data_));
main_win = make_window();
prefs_win = make_preferences_window();} {}
  }
  Function {~AppUI()} {open
  } {
    code {delete rec_img_;
delete stop_img_;} {}
  }
  Function {quit()} {open
  } {
    code {prefs_win->hide();
main_win->hide();} {}
  }
  Function {load_prefs(const microb::s_config& config)} {open return_type void
  } {
    code {set_choice_by_value(pref_audio_device, config.device_index);
set_choice_by_value(pref_sample_rate, config.sample_rate);
set_choice_by_value(pref_channels, config.channels);
pref_http_url->value(config.http_url.c_str());
pref_http_username->value(config.http_username.c_str());
pref_http_password->value(config.http_password.c_str());
set_choice_by_value(pref_codec, config.codec.c_str());
set_choice_by_value(pref_bitrate, config.bitrate);
saved_config_ = config;} {}
  }
  Function {get_prefs()} {open return_type {microb::s_config}
  } {
    code {microb::s_config config;
config.device_index = pref_audio_device->mvalue()->argument();
config.sample_rate = pref_sample_rate->mvalue()->argument();
config.channels = pref_channels->mvalue()->argument();
config.http_url = pref_http_url->value();
config.http_username = pref_http_username->value();
config.http_password = pref_http_password->value();
config.codec = (char *)pref_codec->mvalue()->user_data();
config.bitrate = pref_bitrate->mvalue()->argument();
return config;} {}
  }
  Function {reset_prefs()} {open return_type void
  } {
    code {load_prefs(saved_config_);} {}
  }
  Function {apply_prefs()} {open
  } {
    code {microb::s_config config = get_prefs();
util::Status status = config.check();
if (!status.ok()) {
  fl_alert("Invalid configuration!\\n%s", status.what());
} else {
  prefs_win->hide();
  pipeline_->set_config(config);
}} {}
  }
  Function {set_broadcast_state(bool active)} {open return_type void
  } {
    code {broadcast_active_ = active;
if (active) {
	broadcast_btn->image(stop_img_);
} else {
	broadcast_btn->image(rec_img_);
}} {}
  }
  decl {microb::PipelineManager *pipeline_;} {protected local
  }
  decl {microb::s_config saved_config_;} {protected local
  }
  data rec_png_data_ {protected local filename {rec.png}
  }
  data stop_png_data_ {protected local filename {stop.png}
  }
  decl {Fl_Image *rec_img_;} {protected local
  }
  decl {Fl_Image *stop_img_;} {protected local
  }
  decl {bool broadcast_active_;} {protected local
  }
} 

decl {\#undef Status} {public global
} 

decl {AppUI *app;} {public local
} 

decl {extern void set_choice_by_value(Fl_Choice *, const void *);} {public global
} 

decl {extern void set_choice_by_value(Fl_Choice *, long);} {public global
} 

decl {extern int ui_error_callback(util::Status);} {public global
} 

decl {extern void ui_state_callback(int);} {selected public global
} 
