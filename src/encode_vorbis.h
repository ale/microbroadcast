// -*- mode: c++ -*-
/*

  encode_vorbis.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_encode_vorbis_H
#define __microb_encode_vorbis_H 1

#include <vorbis/vorbisenc.h>

#include "encode.h"
#include "encode_ogg.h"
#include "params.h"
#include "util.h"

namespace microb {

class VorbisEncoder : public Encoder, public OggStreamMixin {
public:
  VorbisEncoder() : buf_((char *)calloc(1, 4 * 4096)) {}

  virtual ~VorbisEncoder() {
    close();
    free(buf_);
  }

  virtual int buffer_size() const { return 4096; }

  virtual const char *content_type() const {
    return OggStreamMixin::content_type();
  }

  util::Status initialize(const EncodingParams &params) {
    params_ = params;

    vorbis_info_init(&vi_);

    int ret =
        vorbis_encode_init(&vi_, params_.channels, params_.sample_rate,
                           params_.bitrate, params_.bitrate, params_.bitrate);
    if (ret < 0) {
      return util::Status(ret) << "failure to initialize Vorbis encoder";
    }

    vorbis_comment_init(&vc_);
    vorbis_comment_add_tag(&vc_, "ENCODER", PACKAGE_STRING);
    vorbis_analysis_init(&vd_, &vi_);
    vorbis_block_init(&vd_, &vb_);

    ogg_packet header, header_comm, header_code;
    vorbis_analysis_headerout(&vd_, &vc_, &header, &header_comm, &header_code);
    ogg_stream_packetin(&os_, &header);
    ogg_stream_packetin(&os_, &header_comm);
    ogg_stream_packetin(&os_, &header_code);

    return util::Status(OK);
  }

  virtual void close() {
    vorbis_block_clear(&vb_);
    vorbis_dsp_clear(&vd_);
    vorbis_comment_clear(&vc_);
    vorbis_info_clear(&vi_);
  }

  virtual int encode(const char *input, int frames, char **output) {
    int16_t *input_samples = (int16_t *)input;
    int w = 0;

    if (frames == 0)
      return 0;

    w += flush_stream(buf_);

    // Obtain a pointer to the float sample buffers required by the
    // Vorbis encoder, and convert our int16 samples to it.
    float **vorbis_buf = vorbis_analysis_buffer(&vd_, frames);
    if (params_.channels == 2) {
      for (int i = 0; i < frames; i++) {
        vorbis_buf[0][i] = input_samples[i * 2] / 32768.0;
        vorbis_buf[1][i] = input_samples[i * 2 + 1] / 32768.0;
      }
    } else {
      for (int i = 0; i < frames; i++) {
        vorbis_buf[0][i] = input_samples[i] / 32768.0;
      }
    }
    vorbis_analysis_wrote(&vd_, frames);

    ogg_packet op;
    while (vorbis_analysis_blockout(&vd_, &vb_) == 1) {
      vorbis_analysis(&vb_, NULL);
      vorbis_bitrate_addblock(&vb_);
      while (vorbis_bitrate_flushpacket(&vd_, &op)) {
        w += write_ogg_packet(buf_ + w, &op);
      }
    }

    *output = buf_;
    return w;
  }

protected:
  vorbis_info vi_;
  vorbis_comment vc_;
  vorbis_block vb_;
  vorbis_dsp_state vd_;
  char *buf_;
  EncodingParams params_;
};

} // namespace

#endif
