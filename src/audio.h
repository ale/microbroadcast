// -*- mode: c++ -*-
/*

  audio.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_audio_H
#define __microb_audio_H 1

#include <memory>
#include <vector>

#include <portaudio.h>

#include "params.h"
#include "ringbuffer.h"
#include "util.h"

namespace microb {

struct DeviceInfo {
  int index;
  std::string name;
  const PaDeviceInfo *info;
};

extern std::vector<DeviceInfo> input_devices;

class Capture {
public:
  Capture();

  virtual ~Capture();

  util::Status open(const Params &params);

  void close();

  int read(char *buf, unsigned int len) { return ring_buffer_->read(buf, len); }

  const Params &params() { return cur_params_; }

protected:
  static int portaudio_callback(const void *inputBuffer, void *outputBuffer,
                                unsigned long framesPerBuffer,
                                const PaStreamCallbackTimeInfo *timeInfo,
                                PaStreamCallbackFlags statusFlags,
                                void *userData);

  static void finished_callback(void *userData);

  int callback(const void *inputBuffer, void *outputBuffer,
               unsigned long framesPerBuffer,
               const PaStreamCallbackTimeInfo *timeInfo,
               PaStreamCallbackFlags statusFlags);

  void stream_finished();

  std::unique_ptr<RingBuffer> ring_buffer_;
  Params cur_params_;
  PaStream *stream_;
  util::Flag stop_;
  util::WaitableFlag done_;
};

void audio_initialize();
void audio_close();

} // namespace

#endif
