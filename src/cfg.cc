/*

  cfg.cc
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "cfg.h"
#include "encode.h"
#include <fstream>
#include <iostream>
#include <stdio.h>

#include <portaudio.h>

using namespace std;
using namespace nlohmann;

#define DEFAULT_CODEC ENC_MP3
#define DEFAULT_SAMPLE_RATE 44100
#define DEFAULT_CHANNELS 2
#define DEFAULT_BITRATE 128000

namespace microb {

static int config_int(const nlohmann::json &jc, const char *key,
                      int default_value) {
  int res = default_value;
  if (jc.find(key) != jc.end()) {
    res = jc[key];
  }
  return res;
}

static string config_str(const nlohmann::json &jc, const char *key,
                         const char *default_value) {
  string res(default_value);
  if (jc.find(key) != jc.end()) {
    res = jc[key];
  }
  return res;
}

json s_config::to_json() const {
  json c = {{"codec", codec},
            {"sample_rate", sample_rate},
            {"channels", channels},
            {"device_index", device_index},
            {"bitrate", bitrate},
            {"http_url", http_url},
            {"http_username", http_username},
            {"http_password", http_password}};
  return c;
}

s_config::s_config()
    : codec(""), sample_rate(-1), channels(-1), device_index(-1), bitrate(-1),
      http_url(""), http_username(""), http_password("") {}

s_config::s_config(const json &jc)
    : codec(config_str(jc, "codec", "")),
      sample_rate(config_int(jc, "sample_rate", -1)),
      channels(config_int(jc, "channels", -1)),
      device_index(config_int(jc, "device_index", -1)),
      bitrate(config_int(jc, "bitrate", -1)),
      http_url(config_str(jc, "http_url", "")),
      http_username(config_str(jc, "http_username", "")),
      http_password(config_str(jc, "http_password", "")) {}

s_config s_config::default_config() {
  s_config config;
  config.codec = DEFAULT_CODEC;
  config.sample_rate = DEFAULT_SAMPLE_RATE;
  config.channels = DEFAULT_CHANNELS;
  config.device_index = Pa_GetDefaultInputDevice();
  config.bitrate = DEFAULT_BITRATE;
  return config;
}

// Merge a config. other has precedence.
s_config s_config::merge(const s_config &other) {
  s_config res;
  res.codec = other.codec.empty() ? codec : other.codec;
  res.sample_rate = (other.sample_rate >= 0) ? other.sample_rate : sample_rate;
  res.channels = (other.channels >= 0) ? other.channels : channels;
  res.device_index =
      (other.device_index >= 0) ? other.device_index : device_index;
  res.bitrate = (other.bitrate >= 0) ? other.bitrate : bitrate;
  res.http_url = other.http_url.empty() ? http_url : other.http_url;
  res.http_username =
      other.http_url.empty() ? http_username : other.http_username;
  res.http_password =
      other.http_password.empty() ? http_password : other.http_password;
  return res;
}

util::Status s_config::check() {
  fprintf(stderr, "check device=%d, sample_rate=%d, channels=%d, bitrate=%d, "
                  "codec=%s, url=%s\n",
          device_index, sample_rate, channels, bitrate, codec.c_str(),
          http_url.c_str());
  if (device_index < 0) {
    return util::Status(ERR) << "no device specified";
  }
  if (sample_rate < 8000 || sample_rate > 48000) {
    return util::Status(ERR) << "sample rate out of range";
  }
  if (channels < 1 || channels > 2) {
    return util::Status(ERR) << "invalid number of channels";
  }
  if (bitrate < 10000 || bitrate > 320000) {
    return util::Status(ERR) << "bitrate out of range";
  }
  if (codec != ENC_MP3 && codec != ENC_OPUS && codec != ENC_VORBIS) {
    return util::Status(ERR) << "unsupported codec";
  }
  if (http_url.empty()) {
    return util::Status(ERR) << "server URL is not set";
  }
  return util::Status(OK);
}

string s_config::user_config_path() {
  char *microb_config_path = getenv("MICROB_CONFIG");
  if (microb_config_path != nullptr) {
    return string(microb_config_path);
  }
  string path;
  char *home = getenv("HOME");
  if (home != nullptr) {
    path = string(home) + "/";
  }
  path += "/.microb.cfg";
  return path;
}

util::Status s_config::read_file(const string &path, s_config *out) {
  json jc;
  ifstream f(path);
  if (!f.is_open()) {
    return util::Status(ERR) << "could not open " << path;
  }
  util::Status ret(OK);
  try {
    f >> jc;
    s_config config(jc);
    *out = config;
  } catch (exception &e) {
    ret = util::Status(ERR) << "JSON error in " << path << ": " << e.what();
  }
  f.close();
  return ret;
}

util::Status s_config::write_file(const string &path) {
  ofstream f(path);
  if (!f.is_open()) {
    return util::Status(ERR) << "could not open " << path;
  }
  f << to_json();
  f.close();
  return util::Status(OK);
}

} // namespace
