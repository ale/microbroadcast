/*

  main.cc
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "config.h"
#include "pipeline.h"
#include "util.h"
#include <getopt.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>

#if ENABLE_GUI
#include "ui.h"
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#endif

using namespace microb;

static void usage() {
  fprintf(
      stderr,
      "Usage: microb [<OPTIONS>] [<CONFIG_FILE>]\n"
      "Simple HTTP streaming tool.\n"
      "\n"
      "Known options:\n"
      "\n"
      "  --url=URL, -u URL          Set the HTTP streaming endpoint\n"
      "  --user=NAME, -U NAME       Username for HTTP authentication\n"
      "  --password=PASS, -P PASS   Password for HTTP authentication\n"
      "  --sample-rate=N, -R N      Sample rate (Hz)\n"
      "  --channels=N, -C N         Number of audio channels\n"
      "  --codec=CODEC, -c CODEC    Audio codec (one of: mp3, vorbis, opus)\n"
      "  --bitrate=N, -b N          Encoding bitrate (bps)\n"
#if ENABLE_GUI
      "  --headless                 Disable GUI\n"
#endif
      "\n"
      "The tool will look for a configuration file in ~/.microb.cfg (or in "
      "the\n"
      "location specified by the MICROB_CONFIG environment variable, if "
      "defined).\n"
      "Alternatively, it is possible to pass a configuration file as a single\n"
      "command-line argument.\n"
      "\n"
      "Configuration files are JSON-encoded dictionary of the above "
      "parameters.\n"
      "\n");
}

static struct option long_options[] = {
    {"help", no_argument, 0, 'h'},
    {"user", required_argument, 0, 'U'},
    {"password", required_argument, 0, 'P'},
    {"url", required_argument, 0, 'u'},

    {"sample-rate", required_argument, 0, 'R'},
    {"channels", required_argument, 0, 'C'},
    {"codec", required_argument, 0, 'c'},
    {"bitrate", required_argument, 0, 'b'},

#if ENABLE_GUI
    {"headless", no_argument, 0, 0},
#endif

    {0, 0, 0, 0}};

// Parse command-line options into a s_config and other global flags.
static int parse_opts(int argc, char **argv, s_config *config,
                      bool *out_enable_gui, int *out_optind) {
  // Set defaults for globals.
  *out_enable_gui = true;
#if defined(linux)
  if (getenv("DISPLAY") == NULL) {
    *out_enable_gui = false;
  }
#endif

  while (1) {
    struct option *opt;
    int option_index = 0;
    int c =
        getopt_long(argc, argv, "hu:U:P:R:C:c:b:", long_options, &option_index);
    if (c < 0)
      break;
    switch (c) {
    case 'h':
      usage();
      exit(0);
    case 'u':
      config->http_url = optarg;
      break;
    case 'U':
      config->http_username = optarg;
      break;
    case 'P':
      config->http_password = optarg;
      break;
    case 'R':
      config->sample_rate = atoi(optarg);
      break;
    case 'C':
      config->channels = atoi(optarg);
      break;
    case 'c':
      config->codec = optarg;
      break;
    case 'b':
      config->bitrate = atoi(optarg);
      break;
    case 0:
      opt = &long_options[option_index];
      if (!strcmp(opt->name, "headless")) {
        *out_enable_gui = false;
      }
      break;
    default:
      // Unknown option. Getopt will have already printed an error message.
      fprintf(stderr, "Run with --help for details.\n");
      return -1;
    }
  }
  *out_optind = optind;
  return 0;
}

// Some process-wide globals.
static s_config arg_config;
static string config_file;

static util::Status make_config(s_config *out_config) {
  s_config config = s_config::default_config();

  // Errors reading the default user configuration file are not fatal.
  s_config user_config;
  string user_config_file(config_file);
  if (user_config_file.empty()) {
    user_config_file = s_config::user_config_path();
  }
  util::Status status = s_config::read_file(config_file, &user_config);
  if (status.ok()) {
    config = config.merge(user_config);
  } else if (!config_file.empty()) {
    return status;
  }

  *out_config = config.merge(arg_config);

  return util::Status(OK);
}

static util::Status reload_config(PipelineManager *pipeline) {
  s_config config;
  util::Status status = make_config(&config);
  if (status.ok()) {
    pipeline->set_config(config);
  }
  return status;
}

static void add_signals(sigset_t *set) {
  sigemptyset(set);
  sigaddset(set, SIGINT);
  sigaddset(set, SIGTERM);
  sigaddset(set, SIGHUP);
}

static void block_signals() {
  sigset_t set;
  add_signals(&set);
  pthread_sigmask(SIG_BLOCK, &set, NULL);
}

#if !defined(_WINDOWS)

// Signals and pthreads do not interact very well in Linux: when using
// signal handlers, one has no control over which thread will run the
// handler. Invoking stop() requires acquiring mutexes and signaling a
// condition, so calling it from a signal handler might cause a
// deadlock. To solve this problem, we block signals on all threads,
// and we run a separate thread to receive signals explicitly using
// sigwait().
static void *signal_handler(PipelineManager *pipeline) {
  sigset_t set;
  add_signals(&set);
  while (true) {
    int sig;
    if (sigwait(&set, &sig) != 0) {
      return 0;
    }
    switch (sig) {
    case SIGINT:
    case SIGTERM:
      fprintf(stderr, "termination requested, exiting...\n");

      // Gracefully stop the pipeline.
      pipeline->close();

// If the UI is active, shut it down too.
#if ENABLE_GUI
      if (app) {
        Fl::lock();
        app->quit();
        Fl::unlock();
      }
#endif

      // Don't catch any more signals as we are tearing down the
      // application and our 'pipeline' pointer might be no longer
      // valid.
      return 0;
    case SIGHUP:
      fprintf(stderr, "received SIGHUP, reloading configuration\n");
      util::Status status = reload_config(pipeline);
      if (!status.ok()) {
        fprintf(stderr, "Error reloading configuration: %s\n", status.what());
      }
      break;
    }
  }
}
#endif

static int headless_error_callback(util::Status status) {
  // Cause the pipeline to terminate.
  fprintf(stderr, "pipeline error: %s\n", status.what());
  return -1;
}

static void run_pipeline(const s_config &config, bool enable_gui) {
  // Create the managed pipeline in an unstarted state, loading the
  // initial configuration. To prevent data races, set the callbacks
  // before starting the pipeline manager background thread.
  unique_ptr<PipelineManager> pipeline(new PipelineManager);

#if ENABLE_GUI
  if (enable_gui) {
    pipeline->set_error_callback(ui_error_callback);
    pipeline->set_state_callback(ui_state_callback);
  } else {
#endif
    pipeline->set_error_callback(headless_error_callback);
#if ENABLE_GUI
  }
#endif

  pipeline->set_config(config);
  pipeline->start_thread(nullptr);

#if !defined(_WINDOWS)
  {
    // Start the signal handler thread. It won't be joined, so create
    // it in a detached state.
    thread sig_thread(signal_handler, pipeline.get());
    sig_thread.detach();
  }
#endif

// If the GUI is enabled, create it and transfer control to the FLTK
// event loop. Leave the pipeline unstarted.
#if ENABLE_GUI
  if (enable_gui) {
    app = new AppUI(pipeline.get());
    app->load_prefs(config);
    app->main_win->show();

    Fl::run();
    // while (Fl::wait() > 0) {
    //   if (Fl::thread_message()) {
    //     /* process your data */
    //     fprintf(stderr, "got thread message\n");
    //   }
    // }

    delete app;
  } else {
#endif
    // Autostart the pipeline and wait for it to terminate (on
    // permanent errors).
    pipeline->start();
    pipeline->wait();
#if ENABLE_GUI
  }
#endif
}

int main(int argc, char **argv) {
  // Block signals before any threads are created, so they all inherit
  // this signal mask.
  block_signals();

#if ENABLE_GUI
  // Acquire the FLTK lock before any other threads are created.
  Fl::lock();
#endif

  // Priority of configuration values, from lower to higher: default
  // configuration, user configuration file in ~/.microb.cfg (or
  // whatever is passed as a command-line argument), command-line
  // flags. But we need to parse the arguments first, to find if a
  // config file was passed on the command line.
  // config_file = s_config::user_config_path();
  int arg_index;
  bool enable_gui;
  if (parse_opts(argc, argv, &arg_config, &enable_gui, &arg_index) < 0) {
    return 1;
  }
  if (arg_index < argc - 1) {
    fprintf(stderr, "Too many arguments. Run with --help for details.\n");
    return 1;
  }
  if (arg_index < argc) {
    config_file = argv[arg_index++];
  }

  // Global initialization - takes a bit of time, so make sure we
  // parse the command-line arguments before so --help is fast.
  audio_initialize();
  http_initialize();

  // The default configuration must be created _after_ having
  // initialized the audio subsystem, so that we can find the default
  // audio input device.
  s_config config;
  util::Status status = make_config(&config);
  if (!status.ok()) {
    fprintf(stderr, "Error reading configuration: %s\n", status.what());
    return 1;
  }

  run_pipeline(config, enable_gui);

  // Final cleanup.
  http_close();
  audio_close();
  return 0;
}
