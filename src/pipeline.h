// -*- mode: c++ -*-
/*

  pipeline.h
  Copyright (C) 2016, <ale@incal.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __microb_pipeline_H
#define __microb_pipeline_H 1

#include <stdio.h>
#include <unistd.h>

#include "audio.h"
#include "cfg.h"
#include "encode.h"
#include "http.h"
#include "util.h"

namespace microb {

// Capture, encode and stream audio. The configuration is specified at
// construction time and it is fixed throughout the lifetime of the
// object. To change it, delete the Pipeline and create a new one.
class Pipeline : public util::JoinableThread {
public:
  Pipeline(const s_config &config) : util::JoinableThread(), config_(config) {}

  virtual ~Pipeline() {
    // Quit when the object goes out of scope. We need to wait until
    // the run thread has exited, to safely destroy the pipeline
    // components.
    close();
    wait();
  }

  // Late initialization of all components, so that we can return a
  // meaningful util::Status if anything goes wrong.
  util::Status start_thread(const util::JoinableThread::callback_t &done_cb) {
    util::Status status = config_.check();
    if (!status.ok()) {
      return status;
    }

    cap_.reset(new Capture());
    status = cap_->open(Params(config_));
    if (!status.ok()) {
      return status;
    }

    Encoder *encptr = nullptr;
    status = make_encoder(config_, &encptr);
    enc_.reset(encptr);
    if (!status.ok()) {
      return status;
    }

    up_.reset(new Uploader(config_, enc_->content_type()));
    status = up_->start_thread(nullptr);
    if (!status.ok()) {
      return status;
    }

    return util::JoinableThread::start_thread(done_cb);
  }

  // close() will definitely be called from another thread.
  // It will cause the main loop to terminate.
  void close() {
    // TODO: do we need a stop flag at all, if we can cause the
    // Capture reader to terminate?
    stop_.set();
    if (cap_) {
      cap_->close();
    }
  }

protected:
  unique_ptr<Capture> cap_;
  unique_ptr<Uploader> up_;
  unique_ptr<Encoder> enc_;
  s_config config_;
  util::Flag stop_;

  // Run the pipeline until an error occurs, or we're asked to exit.
  virtual util::Status run() {
    int bufsz = enc_->buffer_size();
    char *buf = (char *)malloc(bufsz);

    fprintf(stderr, "starting audio pipeline\n");

    util::Status status(OK);
    while (!stop_.is_set()) {
      int n = cap_->read(buf, bufsz);
      if (n < 0) {
        status = util::Status(ERR) << "audio device error";
        break;
      }

      char *encoded = nullptr;
      int frames = n / cap_->params().frame_size();
      int nn = enc_->encode(buf, frames, &encoded);
      if (nn < 0) {
        status = util::Status(ERR) << "encoder error";
        break;
      }
      nn = up_->write(encoded, nn);
      if (nn < 0) {
        status = up_->status();
        break;
      }
    }

    // Close all threaded components now so that the thread joins are
    // actually asynchronous (the destructor calls close() anyway, but
    // in sequence to the wait() call).
    cap_->close();
    up_->close();

    fprintf(stderr, "audio pipeline stopped\n");
    free(buf);
    if (stop_.is_set()) {
      return util::Status(OK);
    }
    return status;
  }
};

#define STATE_PAUSED 0
#define STATE_RUNNING 1

// Control message for the pipeline manager.
class Message {
public:
  enum code_t { UNKNOWN, CLOSE, STOP, START, ERROR, SET_CONFIG } code;

  struct {
    util::Status status;
    const Pipeline *pipeline;
  } error;
  s_config config;

  Message(code_t code_) : code(code_) {}
  Message(code_t code_, const util::Status &status_, const Pipeline *pipeline_)
      : code(code_) {
    error.status = status_;
    error.pipeline = pipeline_;
  }
  Message(code_t code_, const s_config &config_)
      : code(code_), config(config_) {}
};

// The pipeline manager creates and destroys a pipeline when
// necessary, according to the play/pause state, and nicely deals with
// failures.
//
// Error handling behavior can be customized by providing a callback
// which can cause the manager to exit if it returns a negative value.
//
// Synchronization with an external component (such as the UI) can be
// achieved by passing a callback that will be invoked on every state
// change (between STATE_PAUSED and STATE_RUNNING).
//
class PipelineManager : public util::JoinableThread {
public:
  // Type for the error callback function.
  typedef std::function<int(util::Status)> error_callback_t;

  // Type for the state change callback function.
  typedef std::function<void(int)> state_callback_t;

  PipelineManager() : util::JoinableThread(), error_cb_(nullptr) {}

  virtual ~PipelineManager() {
    close();
    wait();
  }

  void set_error_callback(error_callback_t f) { error_cb_ = f; }
  void set_state_callback(state_callback_t f) { state_cb_ = f; }

  void set_config(const s_config &config) {
    queue_.push(new Message(Message::SET_CONFIG, config));
  }

  void close() { queue_.push(new Message(Message::CLOSE)); }

  void start() { queue_.push(new Message(Message::START)); }

  void stop() { queue_.push(new Message(Message::STOP)); }

protected:
  // Notify the error callback. If the return value is < 0, the
  // pipeline manager should quit.
  int notify_error(const util::Status &status) {
    if (error_cb_) {
      return error_cb_(status);
    }
    return 0;
  }

  // Notify the state callback.
  void notify_state(int state) {
    if (state_cb_) {
      state_cb_(state);
    }
  }

  util::Status start_pipeline() {
    pipeline_.reset(new Pipeline(config_));

    // Lambda with capture to retain access to queue_. The pipeline
    // pointer is passed as an argument, to identify out-of-order
    // errors.
    return pipeline_->start_thread([&](const util::Status &pipeline_status) {
      // Report an error from the running pipeline.
      if (!pipeline_status.ok()) {
        queue_.push(
            new Message(Message::ERROR, pipeline_status, pipeline_.get()));
      }
    });
  }

  // Tiny state machine to manage running/pause controls.  It is
  // self-contained, so it doesn't need to take locks (except for the
  // implicit one on the queue).
  util::Status run() {
    int state = STATE_PAUSED;
    notify_state(state);

    while (true) {
      std::unique_ptr<Message> m(queue_.pop());
      switch (m->code) {

      case Message::CLOSE:
        // Delete the pipeline now, instead of relying on the class
        // destructor, because the pipeline completion callback holds
        // a reference to this object!
        pipeline_.reset(nullptr);
        return util::Status(OK);

      case Message::START:
        if (state == STATE_PAUSED) {
          util::Status status = start_pipeline();
          if (status.ok()) {
            state = STATE_RUNNING;
          } else {
            if (notify_error(status) < 0) {
              return status;
            }
          }
        }
        break;

      case Message::STOP:
        if (state == STATE_RUNNING) {
          state = STATE_PAUSED;
          pipeline_.reset(nullptr);
        }
        break;

      case Message::ERROR:
        // Check if the pipeline reporting the error is the running
        // one, otherwise this is a callback from an explicitly
        // terminated pipeline and we're going to ignore it.
        //
        // This check isn't very good - I don't think unique_ptr must
        // necessarily return the same value every time from
        // get(). Perhaps a pipeline ID is required.
        if (state == STATE_RUNNING && pipeline_ &&
            pipeline_.get() == m->error.pipeline) {
          state = STATE_PAUSED;
          pipeline_.reset(nullptr);
          if (notify_error(m->error.status) < 0) {
            return m->error.status;
          }
        } else {
          fprintf(stderr, "pipeline: spurious error reported, pipeline=%p, "
                          "cur_pipeline=%p\n",
                  (void *)m->error.pipeline, (void *)pipeline_.get());
        }
        break;

      case Message::SET_CONFIG:
        // If the pipeline is running, setting a configuration will
        // cause it to restart.
        config_ = m->config;
        if (state == STATE_RUNNING) {
          util::Status status = start_pipeline();
          if (!status.ok()) {
            state = STATE_PAUSED;
            if (notify_error(status) < 0) {
              return status;
            }
          }
        }
        break;

      default:
        fprintf(stderr, "pipeline: unknown message type %d\n", m->code);
      }

      // State may have changed, notify in any case.
      notify_state(state);
    }
  }

  util::Queue<Message *> queue_;

  std::unique_ptr<Pipeline> pipeline_;
  s_config config_;
  error_callback_t error_cb_;
  state_callback_t state_cb_;
};

} // namespace

#endif
